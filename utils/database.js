import mongoose from "mongoose";
let isConnected = false;
export const connectToDb = async () => {
  mongoose.set("strictQuery", true);
  if (isConnected) {
    console.log("Mongose mufaqatli ornatildi");
    return;
  }
  try {
    mongoose.connect(process.env.MONGODB_URI, {
      dbName: "share_Promptopia",
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    isConnected = true;
    console.log("MongoDb connected");
  } catch (error) {
    console.log(error);
  }
};
