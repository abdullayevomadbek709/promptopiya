"use client";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { signIn, signOut, useSession, getProviders } from "next-auth/react";
const Nav = () => {
  const { data: session } = useSession();
  const [providers, setProviders] = useState(null);
  const [toggleDropdown, setToggleDropdown] = useState(false);

  useEffect(() => {
    const setUpProviders = async () => {
      const response = await getProviders();
      setProviders(response);
    };
    setUpProviders();
  }, []);
  return (
    <nav className=" flex-between  w-full mb-16 pt-3">
      <Link href={"/"} className=" flex gap-2 flex-center">
        <Image src={"/assets/images/logo.svg"} alt="Logo Promptopiya" width={30} height={30} />
        <p className=" logo_text">Promptopiya</p>
      </Link>

      {/* Descktop navigation */}

      <div className=" sm:flex hidden">
        {session?.user ? (
          <div className=" flex gap-3 md:gap-5">
            <Link href={"/create-prompt"} className="black_btn">
              Create Post
            </Link>
            <button type="button" className="outline_btn" onClick={signOut}>
              Sing Out
            </button>
            <Link href={"/profile"}>
              <Image src={session?.user.image} className=" rounded-full  shadow-xl shadow-gray-500" alt="Profile Logo" width={37} height={37} />
            </Link>
          </div>
        ) : (
          <>
            {providers &&
              Object.values(providers).map((provider) => {
                return (
                  <button type=" button" onClick={() => signIn(provider.id)} className=" black_btn" key={provider.name}>
                    Sign In
                  </button>
                );
              })}
          </>
        )}
      </div>

      {/* Mobile navigation */}

      <div className=" sm:hidden flex relative">
        {session?.user ? (
          <div className="flex">
            <Image src={session?.user.image} className=" rounded-full  shadow-xl shadow-gray-600" alt="Profile Logo" width={37} height={37} onClick={() => setToggleDropdown((prev) => !prev)} />
            {toggleDropdown && (
              <div className="dropdown">
                <Link href={"/profile"} className="dropdown_link" onClick={() => setToggleDropdown(false)}>
                  My Profile
                </Link>{" "}
                <Link href={"/create-prompt"} className="dropdown_link" onClick={() => setToggleDropdown(false)}>
                  Create Prompt
                </Link>
                <button
                  className=" mt-5 w-full black_btn"
                  type="button"
                  onClick={() => {
                    setToggleDropdown(false);
                    signOut();
                  }}
                >
                  Sign Out
                </button>
              </div>
            )}
          </div>
        ) : (
          <>
            {providers &&
              Object.values(providers).map((provider) => {
                return (
                  <button type=" button" onClick={() => signIn(provider.id)} className=" black_btn" key={provider.name}>
                    Sign In
                  </button>
                );
              })}
          </>
        )}
      </div>
    </nav>
  );
};

export default Nav;
