import { connectToDb } from "@utils/database";
import Prompt from "@models/prompt";
// GET (read)
export const GET = async (request, { params }) => {
  try {
    await connectToDb();
    const prompt = await Prompt.findById(params.id).populate("creator");
    if (!prompt) {
      return new Response("Prompt not found ", { status: 404 });
    }
    return new Response(JSON.stringify(prompt), { status: 200 });
  } catch (error) {
    return new Response("Failed get prompts", { status: 500 });
  }
};

// PATCH update
export const PATCH = async (request, { params }) => {
  const { prompt, tag } = await request.json();

  try {
    await connectToDb();
    const exisitingPrompt = await Prompt.findById(params.id);

    if (!exisitingPrompt) {
      return new Response("Prompt not found", { status: 404 });
    }
    exisitingPrompt.prompt = prompt;
    exisitingPrompt.tag = tag;

    await exisitingPrompt.save();

    return new Response(JSON.stringify(exisitingPrompt), { status: 200 });
  } catch (error) {
    return new Response("Failed to update prompt", { status: 500 });
  }
};

// DELETE

export const DELETE = async (request, { params }) => {
  try {
    await connectToDb();
    await Prompt.findByIdAndDelete(params.id);
    return new Response("Prompt deleted", { status: 200 });
  } catch (error) {
    return new Response("Failed delete to prompt", { status: 500 });
  }
};
