"use client";

import { useEffect, useState } from "react";
import { useSearchParams } from "next/navigation";

import Profile from "@components/Profile";

const UserProfile = () => {
  const searchParams = useSearchParams();
  const userName = searchParams.get("name") || "User";

  const [userPosts, setUserPosts] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const response = await fetch(`/api/users/${searchParams.get("id")}/posts`);
        if (!response.ok) {
          throw new Error("Failed to fetch user posts");
        }
        const data = await response.json();
        setUserPosts(data);
      } catch (error) {
        console.error("Error fetching user posts:", error);
        setError("Failed to fetch user posts. Please try again later.");
      }
    };

    if (searchParams.get("id")) fetchPosts();
  }, [searchParams]);

  if (error) {
    return <div>Error: {error}</div>;
  }

  return <Profile name={userName} desc={`Welcome to ${userName}'s personalized profile page. Explore ${userName}'s exceptional prompts and be inspired by the power of their imagination`} data={userPosts} />;
};

export default UserProfile;
